# YPP2

Simplified Common Lisp refference: http://jtra.cz/stuff/lisp/sclr/index.html

## Základ

Hodnota `nil` je jediná nepravdivá hodnota, pro pravdivou hodnotu se používá `t`.

Symbol může mít 2 vazby – funkční a hodnotovou. To znamená, že může zastávat jak název funkce, tak název proměnné.


## Funkce

`defvar` slouží pro definici (globální) proměnné, lze je předefinovat makrem `setf`

```common-lisp
(defvar *a* '(1 2 3))
```

`setf` slouží pro nastavení aktuální hodnoty vazby symbolu (proměnné)

```common-lisp
(setf *a* (cons 1 '()))
```

`defun` slouží pro definici nové funkce

```common-lisp
(defun soucet (x y) (+ x y))
```

```
(defun function-name lambda-list form*) > function-name
```

`funcall` slouží pro volání funkce, která je uložena v proměnné

```common-lisp
(defun comp (a b)
    (lambda (x)
        (funcall a (funcall b x))))
```

`function` nebo `#'` slouží pro zavolání funkční vazby symbolu

```common-lisp
(comp (function car) (function cdr))

(comp #'car  #'cdr)
```

`lambda` slouží pro vytvoření anonymní funkce

```
(lambda lambda-list form*) > result

lambda-list: obyčejný λ-seznam form: forma
result: výsledná funkce
```

`apply` podobně jako ve scheme aplikuje funkci na daný seznam

```common-lisp
(apply #'+ '(1 2 3 4 5 6 7 8 9 10))
```

`progn` postupně vyhodnocuje podvýrazy a jako výsledek vrátí podvýraz **exprn**

``` lisp
(progn expr1
    .
    .
    .
    expr2
    exprn)
```

`dotimes`

``` lisp
(dotimes (x 5)
    (print x))
```

`dolist`

``` lisp
(dotimes (x '(1 2 3))
    (print x))
```

`let` definice vazeb

```common-lisp
(let ((var init-form)*) form*) > result

var: symbol
init-form: forma
form: forma
result: hodnota vrácená poslední formou form
```

### Práce se seznamy

`list` vytváří nové seznamy

`copy-list` kopíruje seznam

`append` spojuje libovolný počet seznamů

```common-lisp
(append '(1 2 3) '(3 4 5))
```

`last` vrací poslední prvek seznamu

`butlast` vrací první prvek seznamu

`first` `second` `third` `fourth` `fifth` `sixth` `seventh` `eighth` `nineth` `tenth` vrací nth prvek seznamu

`reverse` slouží k obrácení seznamu

```common-lisp
(reverse '(3 2 1))
> (1 2 3)
```

`mapcar` je základní funkce, která aplikuje danou funkci na všechny prvky seznamu a shromažďuje výsledky volání do nového seznamu

```common-lisp
(mapcar (lambda (x) (expt 2 x)) '(2 4 6))
> (4 16 36)

(mapcar #'+ '(1 2 3) '(4 5 6) '(7 8 9))
> (12 15 18)

(mapcar #'cons '(a b c) '(1 2 3))
> ((A . 1) (B . 2) (C . 3))
```

`find` a `find-if`

```
(find element list) > result (find-if predicate list) > result

element: libovolný objekt predicate: funkce
list: seznam
result: nalezený prvek nebo nil

find používá eql, proto (find (cons 1 2) '((1 . 2) (3 . 4))) > nil
```

```common-lisp
(find 2 '(1 2 3))
> 2

(find 4 '(1 2 3))
> nil

(find-if (lambda (x) (> x 4)) '(2 4 6 8))
> 6
```

`length` zjišťuje délku seznamu

`every` testuje, zda všechny prvky posloupnosti vyhovují danému predikátu

## Řízení běhu

`if` klasické if větvění

```common-lisp
(if test-form then-form else-form)
> result
```

`when` pokud je to pravda, tak vyhodnoť, nebo `nil`

```common-lisp
(when (= 2 2) 'jupi)
> jupi
```

`unless` pokud to není pravda, tak vyhodnoť, nebo `nil`

```common-lisp
(unless (= 2 2) 4)
> nil
```

## Logické operátory

`not` neguje svůj argument a mohla by být definována takto:

```common-lisp
(defun not (x)
    (unless x t))
```

`null` je ekvivalentem `not`, testuje, zda-li se jedná o prázdný seznam

`and` a `or` jsou dobře známé, zajímavé je, že mohou řídit běh programu. V případě `and` je vyhodnocen poslední výraz (protože se musí zkontrolovat, jestli všechny výrazy jsou `true`) a u `or` je vyhodnocen první pravdivý výraz (protože všechny po něm není již potřeba vyhodnocovat).

```common-lisp
(and (= 1 2) nil '(vyhodnoti se ten napravo ode me) 666)
```

```common-lisp
(or (= 1 2) nil '(vyhodnotim se ja) 666 'nazdar)
```

## Porovnávání

`eql` slouží k porovnávání na téže fyzické adrese

```common-lisp
(eql 1 1)
> T

(eql 1 2)
> nil

(eql 1 1.0)
> nil

(eql (cons 1 2) (cons 1 2))
> nil
```

## Predikáty

`numberp` je predikát pro číslo

`consp` predikát pro tečkový pár

`listp` predikát pro pár nebo nil

`typep` predikát typu může testovat například (`symbol`  `function` `cons` `number` `integer` `string`)

```common-lisp
(typep (lambda (x) (+ x 1)) 'function)
> T

(typep "abc" 'string)
> T

(setf type 'number)
> NUMBER

(typep 10 type)
> T

(typep 10 nil)
> NIL
```

## Další symboly do základní výbavy

`=`
`/=`
`<=`
`>`
`min`
`max`
`abs`
`signum`
`sin`
`cos`
`tan`
`exp`
`expt`
`sqrt`
`log`
`pi`
`floor`
`ceiling`
`round`
`truncate`
`mod`
`rem`

## Chyby

`error` signalizuje chybu, jejíž popis je v řetězci string. Současně
dojde k předčasnému zastavení běhu programu.

```common-lisp
(error string)
```

## Textový výstup

`print` vrátí svou hodnotu a zobrazí výsledek v listeneru / outputu

```common-lisp
(print (list (cons 1 2) (cons 1 2)))
> ((1 . 2) (1 . 2))
```

`format` slouží k zanesení proměnných do textu

`t` - vytiskne do standardního výstupu
`nil` - vytiskne jako string

`~%` - nový řádek
`~s` - simple print
`~a` - aesthetic print

```common-lisp
(format t "~%List ~s and string ~s" (list 1 1) "Ahoj")

> List (1 1) and string "Ahoj"
```

## Listener

`*` - předchozí výsledek

`**` - výsledek před `*`

`***` - výsledek před `**`